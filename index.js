const puppeteer = require("puppeteer");

const nbSlides = 39;
const url = (page) => `http://localhost:8080/#/${page}`;

const padLength = nbSlides.toString().length;

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.setViewport({
    width: 1440,
    height: 1080,
    deviceScaleFactor: 1,
  });

  for (let i = 0; i < nbSlides; i++) {
    const pageNumber = (i + 1).toString().padStart(padLength, "0");
    console.log(`Loading page ${pageNumber}`);
    await page.goto(url(i));
    await page.screenshot({path: `output/${pageNumber}.png`});
  }

  await browser.close();
})();
